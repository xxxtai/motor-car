#include <MsTimer2.h>
#include <Timer.h>
#include <Servo.h>   //舵机驱动库
#include <EasyStepper.h>  //步进电机驱动库
#include <SoftwareSerial.h>

#define  STEP_A_PIN 2  //设定步进电机A速度"PUL+"在arduino的8脚
#define DIR_A_PIN 4  //设定步进电机A方向"DIR+"在9脚
#define EN_A_PIN 3  //设定步进电机A使能"EN"在10脚
#define DIR_PIN_INVERTED true
#define EN_PIN_INVERTED true
EasyStepper stepperA(STEP_A_PIN, DIR_A_PIN, EN_A_PIN, DIR_PIN_INVERTED,DIR_PIN_INVERTED);
#define LEFT_F 10
#define LEFT_B 11
#define RIGHT_F 8
#define RIGHT_B 9
#define INFRAREAD 7
SoftwareSerial mySerial(2, 3); // RX, TX
bool firstTime = true;
bool foundStartFlag = false;
bool infrareadException = false;
const char START_FLAG = '&'; 
const char END_FLAG = '#'; 
char buf[11];
unsigned char index = 0;
long count = 0;
unsigned char tick_5ms = 0;

void flash(){
    tick_5ms++;
}

void setup(){
    Serial.begin(57600, SERIAL_8N1);
    while (!Serial) {}
    mySerial.begin(57600);
    
    pinMode(LEFT_F, OUTPUT);
    pinMode(LEFT_B, OUTPUT);
    pinMode(RIGHT_F, OUTPUT);
    pinMode(RIGHT_B, OUTPUT);
    pinMode(INFRAREAD, INPUT);
    stepperA.debugMode(false);
    stepperA.startup();
    Serial.println("start");
}

void loop(){
//    if(firstTime){
//      firstTime = false;
//      unload();
//    }
    if(digitalRead(INFRAREAD) == LOW){
          infrareadException = true;
          analogWrite(LEFT_F, 0);
          analogWrite(LEFT_B, 0);
          analogWrite(RIGHT_F, 0);
          analogWrite(RIGHT_B, 0);
    }else {
          infrareadException = false;
    }  
    if(Serial.available() > 0) {
        char data = Serial.read();
        
        if(!foundStartFlag && data == START_FLAG){
            foundStartFlag = true;
        }else if(foundStartFlag && data != END_FLAG){
            if(index >= 11) {
                foundStartFlag = false;
                index = 0;
            } else {
                buf[index++] = data;
            }
        }else if(foundStartFlag && data == END_FLAG){
            if(index == 11 && buf[0] == 86 && buf[1] == 61 && buf[6] == 47 && (buf[2] == 43 || buf[2] == 45 || buf[2] == 117) && (buf[7] == 43 || buf[7] == 45 || buf[7] == 117)){
//                mySerial.write(buf);
                char v_l = 0;
                for(char i = 3; i < 6; i++) {
                    if(buf[i] != 48) {
                        v_l += (pow(10, 5-i)*(buf[i] - 48));
                    }
                }
                if(buf[3] != 48){
                    v_l++;
                }
                
                char v_r = 0;
                for(char i = 8; i < 11; i++) {
                    if(buf[i] != 48) {
                        v_r += (pow(10, 10-i)*(buf[i] - 48));
                    }
                }
                if(buf[8] != 48){
                    v_r++;
                }
                                
                if(!infrareadException || (v_l == 75 && v_r == 75) || (v_l == 0 && v_r == 0)) {
                    if(buf[2] == 43){
                        analogWrite(LEFT_F, v_l);
                        analogWrite(LEFT_B, 0);
                    } else if(buf[2] == 45){
                        analogWrite(LEFT_F, 0);
                        analogWrite(LEFT_B, v_l);
                    }
                
                    if(buf[7] == 43){
                        analogWrite(RIGHT_F, v_r);
                        analogWrite(RIGHT_B, 0);
                    } else if(buf[7] == 45){
                        analogWrite(RIGHT_F, 0);
                        analogWrite(RIGHT_B, v_r);
                    }
                    
                    if(buf[2] == 117 && buf[7] == 117){
                        unload();
                    }
                }
            }
            foundStartFlag = false;
            index = 0;
        }
    }
}

void unload(){
      stepperA.rotate(800, 4500, 1);//1000step == 2.5cm, -  forward
      while (!stepperA.isDone()) { 
          stepperA.run();
      }
      
      stepperA.rotate(800, -4500, 1);
      while (!stepperA.isDone()) { 
          stepperA.run();
      }
}
